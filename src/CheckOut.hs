module CheckOut(calculate, sumPrices) where

import Model
import Data.List
import Data.Maybe

calculate :: [PriceRule] -> [Sku] -> Maybe Int
calculate rules skus = calc rules skus (Just 0)

calc :: [PriceRule] -> [Sku] -> Maybe Int -> Maybe Int
calc rules [] total = total
calc rules skus total = cheapest
  where
  applicableRules = filter (\r -> applicable r skus) rules
  appliedRules = fmap (\r -> calc rules (applyRule r skus) (sumPrices (total : Just (price r) : []))) applicableRules
  cheapest = case (filter (\r -> r/=Nothing) appliedRules) of
    [] -> Nothing
    ar -> Just $ minimum $ map fromJust ar

sumPrices :: [Maybe Int] -> Maybe Int
sumPrices prices = fmap sum $ sequence prices

applicable :: PriceRule -> [Sku] -> Bool
applicable (UnitPriceRule sku price) skus = [sku] `isInfixOf` skus
applicable (MultiBuyPriceRule skuList price) skus = skuList `isInfixOf` skus

applyRule :: PriceRule -> [Sku] -> [Sku]
applyRule (UnitPriceRule sku price) skus = delete sku skus
applyRule (MultiBuyPriceRule skuList price) skus = remove skuList skus

remove :: [String] -> [String] -> [String]
remove [] bs = bs
remove (a:[]) bs = delete a bs
remove (a:as) bs = remove as $ delete a bs
