import System.Environment

import Model
import CheckOut

rules = [UnitPriceRule "A" 50,
             UnitPriceRule "B" 50,
             UnitPriceRule "C" 50,
             UnitPriceRule "D" 50,
             MultiBuyPriceRule ["A", "A", "A"] 130,
             MultiBuyPriceRule ["B", "B"] 45]

main :: IO ()
main = do
  s <- getLine
  parseInput [] s

parseInput :: [Sku] -> String -> IO ()
parseInput _ "exit" = putStrLn "bye"
parseInput skuList "=" = do
  _ <- putStrLn $ show $ calculate rules skuList
  s <- getLine
  parseInput skuList s

parseInput skuList sku = do
  _ <- putStrLn $ show $ calculate rules (sku : skuList)
  s <- getLine
  parseInput skuList s
