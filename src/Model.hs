module Model(PriceRule(..), Sku) where

type Sku = String

data PriceRule = UnitPriceRule { sku :: Sku, price :: Int} | MultiBuyPriceRule { skuList :: [Sku], price :: Int } deriving (Show, Eq)
