import Test.Hspec
import CheckOut
import Model

main :: IO ()
main = hspec spec

spec :: Spec
spec = do

  describe "Checkout sumPrices" $ do
    it "Should correctly sum Justs" $ do
      sumPrices [Just 10, Just 10] `shouldBe` Just 20

    it "Should correctly sum a Just and Nothing" $ do
      sumPrices [Just 10, Nothing] `shouldBe` Nothing

  describe "CheckOut" $ do
    it "Should calculate the correct total for a single item" $ do
      calculate [UnitPriceRule "A" 10] ["A"]
      `shouldBe` Just 10

    it "Should calculate the correct total for a list of items" $ do
      calculate [UnitPriceRule "A" 10] ["A", "A", "A"]
      `shouldBe` Just 30

    it "Should calculate the correct total for mixed items" $ do
      calculate [UnitPriceRule "A" 10, UnitPriceRule "B" 15] ["A", "B"]
      `shouldBe` Just 25

    it "Should calculate the correct total for a multi-buy item" $ do
      calculate [MultiBuyPriceRule ["A", "A"] 15] ["A", "A"]
      `shouldBe` Just 15

    it "Should calculate the correct total for a multi-buy and Unit combination" $ do
      calculate [MultiBuyPriceRule ["A", "A"] 15, UnitPriceRule "A" 10] ["A", "A", "A"]
      `shouldBe` Just 25

    it "Should calculate the correct total for a swapped multi-buy and Unit combination" $ do
      calculate [UnitPriceRule "A" 10, MultiBuyPriceRule ["A", "A"] 15] ["A", "A", "A"]
      `shouldBe` Just 25

    it "Should calculate the correct total for a mixed multi-buy and Unit combination" $ do
      calculate [MultiBuyPriceRule ["A", "A"] 15,
                 UnitPriceRule "A" 10,
                 UnitPriceRule "B" 20]
                ["A", "A", "A", "B"]
      `shouldBe` Just 45

    it "Should calculate the correct total for mixed multi-buy combinations" $ do
      calculate [MultiBuyPriceRule ["A", "A"] 15,
                 UnitPriceRule "A" 10,
                 MultiBuyPriceRule ["B", "B", "B"] 50,
                 UnitPriceRule "B" 20]
                ["A", "A", "A", "B", "B", "B", "B"]
      `shouldBe` Just 95

    it "Should handle cases where not all items can be purchased" $ do
      calculate [MultiBuyPriceRule ["A", "A"] 15] ["A", "A", "A"]
      `shouldBe` Nothing

{-
    it "Should handle cases where pricerule combinations can have dead-ends" $ do
      calculate [MultiBuyPriceRule ["B", "B", "A"] 20,
                 MultiBuyPriceRule ["A", "A"] 15]
                ["A", "A", "A", "A", "B", "B", "B", "B"]
      `shouldBe` Just 55
-}
